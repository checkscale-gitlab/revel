#![allow(non_snake_case)]

use super::structs::{FileSystemEntry, create_FileSystemEntry_from_DirEntry};

use std::collections::HashSet;

use walkdir::WalkDir;


fn check_DirEntry_extention(entry: walkdir::DirEntry) -> Option<walkdir::DirEntry> {
    let extension = entry.path().extension()?.to_str()?;
    match extension {
        "mp3" => Some(entry),
        "flac" => Some(entry),
        "ogg" => Some(entry),
        "m4a" => Some(entry),
        _ => None,
    }
}

pub fn scan_directory_and_send_to_channel(
    music_directory: String,
    tx: &mut spmc::Sender<FileSystemEntry>,
) -> Result<(), failure::Error> {
    let mut files: u64 = 0;

    // iterate through the directory tree recursively
    for entry in WalkDir::new(music_directory) {

        let mut entry = entry?;
        
        if entry.file_type().is_file() {
            entry = match check_DirEntry_extention(entry) {
                Some(entry) => entry,
                None => continue,
            };
        }

        
        // construct FileSystemEntry struct from DirEntry
        let fs_entry = create_FileSystemEntry_from_DirEntry(entry)?;
        
        files = files + 1;

        // submit fs_entry to queue for processing
        tx.send(fs_entry)?;
    }
    Ok(())
}

pub fn rescan_directory_and_send_to_channel(
    music_directory: String,
    tx: &mut spmc::Sender<FileSystemEntry>,
    existing_entries: HashSet<i64, twox_hash::RandomXxHashBuilder64>,
) -> Result<(), failure::Error> {
    let mut files: u64 = 0;
    let mut new_files: u64 = 0;


    // iterate through the directory tree recursively
    for entry in WalkDir::new(music_directory) {

        let mut entry = entry?;
        
        if entry.file_type().is_file() {
            entry = match check_DirEntry_extention(entry) {
                Some(entry) => entry,
                None => continue,
            };
        }

        // construct FileSystemEntry struct from DirEntry
        let fs_entry = create_FileSystemEntry_from_DirEntry(entry)?;
        
        files = files + 1;

        if existing_entries.contains(&fs_entry.id) {
            continue;
        }

        new_files = new_files + 1;

        // submit fs_entry to queue for processing
        tx.send(fs_entry)?;
    }
    Ok(())
}



