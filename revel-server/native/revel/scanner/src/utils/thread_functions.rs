use super::structs::{FileSystemEntry, DatabaseEntry, scan_FileSystemEntry_into_DatabaseEntry};
use super::db_operations::{insert_DatabaseEntry_into_database};

pub fn tag_parser_thread(
    tx: crossbeam_channel::Sender<DatabaseEntry>,
    rx: spmc::Receiver<FileSystemEntry>,
) {
    let mut files: u64 = 0;
    loop {
        files = files + 1;
        let fs_entry = match rx.recv() {
            Ok(x) => x,
            Err(_) => break,
        };
        let db_entry =
            match scan_FileSystemEntry_into_DatabaseEntry(fs_entry) {
                Ok(x) => x,
                Err(_) => continue,
            };
        tx.send(db_entry).unwrap();
    }
}

pub fn database_insertion_thread(
    rx: crossbeam_channel::Receiver<DatabaseEntry>,
    mut conn: rusqlite::Connection,
) {
    let transaction = conn.transaction().unwrap();
    let mut files: u64 = 0;
    loop {
        files = files + 1;
        let db_entry = match rx.recv() {
            Ok(x) => x,
            Err(_) => break,
        };
        insert_DatabaseEntry_into_database(db_entry, &transaction);
    }
    transaction.commit().unwrap();
}
