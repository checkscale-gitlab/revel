#![allow(non_snake_case)]

use super::structs::DatabaseEntry;

use std::hash::{Hash, Hasher};

use rusqlite::params;

pub fn init_database(conn: &rusqlite::Connection, scan_path: &str) -> Result<(), rusqlite::Error> {

    let mut hasher = twox_hash::XxHash64::with_seed(69);
    scan_path.to_string().hash(&mut hasher);
    let id = hasher.finish() as i64;

    conn.execute_batch(
        "BEGIN;
             CREATE TABLE entities(
                 id          INTEGER NOT NULL UNIQUE,
                 parent      INTEGER NOT NULL,
                 type        TEXT,
                 filepath    TEXT,
                 PRIMARY KEY(ID)
              );
              CREATE TABLE files (
                  id        INTEGER NOT NULL UNIQUE,
                  title     TEXT,
                  artist    TEXT,
                  album     TEXT,
                  trackn    INTEGER NOT NULL,
                  year      INTEGER,
                  genre     TEXT,
                  ext       TEXT,
                  filename  TEXT,
                  PRIMARY KEY(id)
              );
              CREATE TABLE folders (
                  id        INTEGER NOT NULL UNIQUE,
                  filename  TEXT,
                  PRIMARY KEY(id)
              );
              CREATE TABLE users (
                  username         TEXT NOT NULL UNIQUE,
                  \"password\"     TEXT,
                  admin_role       INTEGER,
                  settings_role    INTEGER,
                  download_role    INTEGER,
                  playlist_role    INTEGER,
                  cover_art_role   INTEGER,
                  root             INTEGER,
                  PRIMARY KEY(username)
              );
              CREATE TABLE roots (
                  name      TEXT UNIQUE,
                  entity_id INTEGER NOT NULL UNIQUE
              );
              CREATE TABLE metadata (
                  keyword   TEXT UNIQUE,
                  val       INTEGER
              );
              INSERT INTO metadata (
                  keyword, val
              ) VALUES (
                  'last_scanned', 0
              );
        COMMIT;"
    )?;
    conn.execute(
        "INSERT INTO users (
             username, \"password\", admin_role, settings_role, download_role, playlist_role, cover_art_role, root
         ) VALUES (
             'admin', \"password\", 1, 1, 1, 1, 1, ?
         );",
        params![id]
    )?;
    Ok(())
}

pub fn insert_DatabaseEntry_into_database(db_entry: DatabaseEntry, transaction: &rusqlite::Transaction) {
    let type_str: &str;

    if db_entry.fs_entry.file_type.is_file() {
        transaction
            .execute(
                "INSERT OR REPLACE INTO `files` (
                 `id`, `title`, `artist`, `album`, `trackn`, `year`, `genre`, `ext`, `filename`)
                 VALUES (?,?,?,?,?,?,?,?,?);",
                params![
                    db_entry.fs_entry.id,
                    db_entry.title,
                    db_entry.artist,
                    db_entry.album,
                    db_entry.track,
                    db_entry.year,
                    db_entry.genre,
                    db_entry.fs_entry.extension,
                    db_entry.fs_entry.path,
                ],
            )
            .unwrap();

        type_str = "File";
    } else if db_entry.fs_entry.file_type.is_dir() {
        transaction
            .execute(
                "INSERT OR REPLACE INTO `folders` (
                 `id`, `filename`)
                 VALUES (?,?);",
                params![
                    db_entry.fs_entry.id,
                    db_entry.title,
                ],
            )
            .unwrap();
        
        type_str = "Folder";
    } else {
        return;
    }

    transaction
        .execute(
            "INSERT OR REPLACE INTO `entities` (
                `id`, `parent`, `type`, `filepath`
            ) VALUES (?,?,?,?);",
            params![
                db_entry.fs_entry.id,
                db_entry.fs_entry.parent_id,
                type_str,
                db_entry.fs_entry.path
            ],
        )
        .unwrap();
}

pub fn create_HashSet_from_database_entries(conn: &rusqlite::Connection) -> Result<std::collections::HashSet<i64, twox_hash::RandomXxHashBuilder64>, rusqlite::Error> {
    let mut stmt = conn.prepare("SELECT id from entities")?;
    let ids = stmt.query_map(rusqlite::NO_PARAMS, |row| {
        row.get::<_, i64>(0)
    })?;

    let mut db_entries = std::collections::HashSet::with_hasher(twox_hash::RandomXxHashBuilder64::default());
    
    for id in ids {
        let id = id?;
        db_entries.insert(id);
    }
    Ok(db_entries)
}
