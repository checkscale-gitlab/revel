# Used by "mix format"
[
  inputs: [
    "{mix,.formatter}.exs",
    "{config,test}/**/*.{ex,exs}",
    "lib/revel/*{ex,exs}",
    "lib/revel/**/*{ex,exs}"
  ]
]
