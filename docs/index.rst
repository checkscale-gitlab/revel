.. Revel documentation master file, created by
   sphinx-quickstart on Sat Nov 16 03:12:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Revel's documentation!
=================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    install/from_source.rst
    install/docker.rst
    install/revel-config.rst

    config/first_run.rst
    config/reverse_proxy.rst

    dev/revel_server.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
